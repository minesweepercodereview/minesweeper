// Minesweeper (console, text-only)



const readline = require('readline');

if (process.argv.length !== 5) {
    console.log('Usage: node minesweeper.js numRows numColumns numMines');
    process.exit(1);
}

const numRows = +process.argv[2];
const numCols = +process.argv[3];
const numMines = +process.argv[4];

if (numRows <= 0 || numRows > 99 || numCols <= 0 || numCols > 99) {
    console.log('Number of rows/columns must be 0-99');
    process.exit(1);
}

if (numMines < 1 || numMines > numRows * numCols) {
    console.log(`Number of mines must be between 1 and numRows * numColums. Max mines for those dimensions is ${numRows * numCols}`);
    process.exit(1);
}

/* Begin init */

    const rl = readline.createInterface({
        "input": process.stdin,
        "output": process.stdout
    });

    const cols = [];
    let numNotRevealed = numRows * numCols - numMines;

    /* Coord order:
        1 2 3
        4   5
        6 7 8
    */
    // To iterate through all adjacent locations of any given (X,Y)
    const coordDeltas = [
        [-1, -1],
        [0, -1],
        [1, -1],
        [-1, 0],
        [1, 0],
        [-1, 1],
        [0, 1],
        [1, 1]
    ];

    // Initialize grid with 0's
    for (let x = 0; x < numCols; x++) {
        let col = [];
        for (let y = 0; y < numRows; y++) {
            col.push({
                "value": 0,
                "revealed": false,
                "label": null
            });
        }
        cols.push(col);
    }

    // Place mines
    for (let i = 0; i < numMines; i++) {
        let mineSet = false;

        while (!mineSet) {
            let x = Math.floor(Math.random() * numCols);
            let y = Math.floor(Math.random() * numRows);
            if (!mineAt(x, y)) {
                setMine(x, y);
                mineSet = true;
            }
        }
    }

    // Adjust values so they equal number of adjacement mines.
    for (let x = 0; x < numCols; x++) {
        for (let y = 0; y < numRows; y++) {
            if (mineAt(x, y) === true) {
                increaseSurroundingValues(x, y);
            }
        }
    }

/* End Init */


function gameLoop() {
    printGrid();

    // Get user move
    rl.question('Make a move: ', (line) => {
        const inputs = line.trim().split(' ');

        if (inputs.length !== 3) {
            console.log('Invalid move format, read instructions.');
        } else {
            const [command, xStr, yStr] = inputs;
            const x = +xStr - 1;
            const y = +yStr - 1;

            if (!coordInBounds(x, y)) {
                console.log('Coord out of bounds!');
            } else {
                makeMove(command, x, y);
            }
        }

        gameLoop();
    });
}

function makeMove(command, x, y) {
    switch (command) {
        case 'reveal':
            if (isRevealed(x, y)) {
                console.log(`(${x+1},${y+1}) is already revealed.`);
            } else if (isLabeledAsMine(x, y)) {
                console.log(`(You already labeled ${x+1},${y+1}) as a mine! Can not reveal when labeled as a mine.`);
            } else if (mineAt(x, y)) {
                printSolution();
                printLosingMessage();
                process.exit(0);
            } else {
                // revealRecursively(x, y, true);
                revealNonRecursively(x, y);
                if (numNotRevealed === 0) {
                    printSolution();
                    printWinningMessage();
                    process.exit(0);
                }
            }
            break;
        case 'mark':
            if (isRevealed(x, y)) {
                console.log("Can't mark a location that's already revealed.");
            } else {
                mark(x, y);
                console.log(`(${x+1}, ${y+1}) was marked.`);
            }
            break;
        case 'mine':
            if (isRevealed(x, y)) {
                console.log("Can't label a location as a mine if it's already revealed.");
            } else {
                mine(x, y);
                console.log(`(${x+1}, ${y+1}) labeled as a mine.`);
            }
            break;
        case 'reset':
            if (isRevealed(x, y)) {
                console.log("This location is already revealed, so there is no label to remove.");
            } else if (!isLabeled(x, y)) {
                console.log(`(${x+1}, ${y+1}) is not labeled. Nothing to reset.`)
            } else {
                reset(x, y);
                console.log(`(${x+1}, ${y+1}) label was removed.`);
            }
        default:
            console.log('Unknown command:', command);
    }
}

function revealRecursively(x, y, isFirstReveal) {
    if (!coordInBounds(x, y)) {
        return;
    }
    if (isRevealed(x, y)) {
        return;
    }
    if (isLabeled(x, y) && !isFirstReveal) {
        return;
    }

    reveal(x, y);

    if (getValue(x, y) === 0) {
        coordDeltas.forEach((coordDelta) => {
            const x2 = x + coordDelta[0];
            const y2 = y + coordDelta[1];
            revealRecursively(x2, y2, false);
        });
    }
}

function revealNonRecursively(xInit, yInit) {
    const stack = [[xInit, yInit]];
    const alreadyVisited = {};

    while (stack.length) {
        let [x, y] = stack.pop();
        reveal(x, y);

        if (getValue(x, y) === 0) {
            coordDeltas.forEach((coordDelta) => {
                const x2 = x + coordDelta[0];
                const y2 = y + coordDelta[1];

                if (alreadyVisited[x2 + ',' + y2] === true) {
                    return;
                }

                if (coordInBounds(x2, y2) && !isRevealed(x2, y2) && !mineAt(x2, y2) && !isMarked(x2, y2)) {
                    stack.push([x2, y2]);
                    alreadyVisited[x2 + ',' + y2] = true;
                }
            });
        }
    }
}

function reveal(x, y) {
    cols[x][y].revealed = true;
    numNotRevealed--;
}

function increaseSurroundingValues(x, y) {
    coordDeltas.forEach((coordDelta) => {
        let x2 = x + coordDelta[0];
        let y2 = y + coordDelta[1];

        if (!coordInBounds(x2, y2) || mineAt(x2, y2)) {
            return;
        }

        setValue(x2, y2, getValue(x2, y2) + 1);
    });
}

function setValue(x, y, value) {
    cols[x][y].value = value;
}

function getValue(x, y) {
    return cols[x][y].value;
}

function coordInBounds(x, y) {
    return x >= 0 && x < numCols && y >= 0 && y < numRows;
}

function mineAt(x, y) {
    return cols[x][y].value === 'M';
}

function setMine(x, y) {
    cols[x][y].value = 'M';
}

function isRevealed(x, y) {
    return cols[x][y].revealed;
}

function isLabeled(x, y) {
    return cols[x][y].label !== null;
}

function isMarked(x, y) {
    return cols[x][y].label === '?';
}

function isLabeledAsMine(x, y) {
    return cols[x][y].label === 'M';
}

function mine(x, y) {
    cols[x][y].label = 'M';
}

function mark(x, y) {
    cols[x][y].label = '?';
}

function reset(x, y) {
    cols[x][y].label = null;
}

function printGrid() {
    let horizontalRuler = '';
    for (let i = 0; i < numCols; i += 2) {
        horizontalRuler += ((i + 1) < 10 ? '0' : '') + (i + 1) + '  ';
    }

    const numDashes = 2 * numCols;
    let dashes = '';
    for (let i = 0; i < numDashes; i++) {
        dashes += '-';
    }

    console.log('  x ' + horizontalRuler);
    console.log('y   ' + dashes);
    for (let y = 0; y < numRows; y++) {
        let rowValues = [];
        for (let x = 0; x < numCols; x++) {
            let coord = cols[x][y];
            let rowValue;
            if (isRevealed(x, y)) {
                rowValue = coord.value;
                if (coord.value === 0) {
                    rowValue = '.';
                }
            } else if (isLabeledAsMine(x, y)) {
                rowValue = 'M';
            } else if (isMarked(x, y)) {
                rowValue = '?';
            } else {
                rowValue = '*';
            }
            rowValues.push(rowValue);
        }
        console.log(((y + 1) < 10 ? '0' : '') + (y + 1) + '| ' + rowValues.join(' '));
    }
}

function printSolution() {
    let horizontalRuler = '';
    for (let i = 0; i < numCols; i += 2) {
        horizontalRuler += ((i + 1) < 10 ? '0' : '') + (i + 1) + '  ';
    }

    const numDashes = 2 * numCols;
    let dashes = '';
    for (let i = 0; i < numDashes; i++) {
        dashes += '-';
    }

    console.log('  x ' + horizontalRuler);
    console.log('y   ' + dashes);

    for (let y = 0; y < numRows; y++) {
        let rowValues = [];
        for (let x = 0; x < numCols; x++) {
            let coord = cols[x][y];
            let rowValue = coord.value;
            if (!mineAt(x, y) && isLabeledAsMine(x, y)) {
                rowValue = '-';
            } else if (rowValue === 0) {
                rowValue = '.';
            } else if (rowValue === 'M') {
                rowValue = ' ';
            }
            rowValues.push(rowValue);
        }
        console.log(((y + 1) < 10 ? '0' : '') + (y + 1) + ') ' + rowValues.join(' '));
    }
}

function printInstructions() {
    console.log('Commands: reveal, mine, mark, reset.');
    console.log('reveal X Y: reveal (X, Y), and if empty, reveal its neighbors. Applied recursively.');
    console.log('mine X Y: label (X, Y) as a mine (M). Locations labeled as mines are protected from accidental reveals.');
    console.log("mark X Y: label (X, Y) with a '?'. This is to help remember which spots might be a mine.");
    console.log('reset X Y: remove mine/mark labels from(X, Y)');
    console.log('Coords are relative to the top-left corner, which is (1,1).');
    console.log('X increases from left to right, and Y increases from top to bottom.');
    console.log("In the solution, for readability, mines are represented as blank spaces (' '), incorrectly assigned mines as a dash ('-')");
}

function printWinningMessage() {
    console.log('You did it! All mines neutralized.');
}

function printLosingMessage() {
    console.log('Oh no! You hit a mine.');
}



// Start game

printInstructions();
gameLoop();
